package org.example.listener;

import lombok.extern.slf4j.Slf4j;
import org.example.event.CatEvent;
import org.example.event.DogEvent;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

/**
 * @Description 事件监听
 * @Author zhouhouliang
 * @Date 2024/2/22 14:30
 */
@Slf4j
@Component
public class EventListenerHolder {

    @Async // 异步
    @EventListener(CatEvent.class)
    public void listenCat(CatEvent event) {
        log.info("事件监听! content:" + event.getContent());
    }

}
