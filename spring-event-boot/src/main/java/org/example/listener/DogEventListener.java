package org.example.listener;

import lombok.extern.slf4j.Slf4j;
import org.example.event.DogEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

/**
 * @Description 事件监听
 * @Author zhouhouliang
 * @Date 2024/2/19 17:55
 */
@Component
@Slf4j
public class DogEventListener implements ApplicationListener<DogEvent> {

    @Async // 异步
    @Override
    public void onApplicationEvent(DogEvent event) {
        log.info("事件监听! content:" + event.getContent());
    }
}
