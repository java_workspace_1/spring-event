package org.example.event;

import org.springframework.context.ApplicationEvent;

/**
 * @Description 自定义事件
 * @Author zhouhouliang
 * @Date 2024/2/19 17:33
 */
public class CatEvent extends ApplicationEvent {

    private String content;


    public CatEvent(String content) {
        super(new Object());
        this.content = "喵喵喵:" + content;
    }

    public String getContent() {
        return content;
    }
}
