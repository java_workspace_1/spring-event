package org.example.event;

import org.springframework.context.ApplicationEvent;

/**
 * @Description 自定义事件
 * @Author zhouhouliang
 * @Date 2024/2/19 17:33
 */
public class DogEvent extends ApplicationEvent {

    private String content;


    public DogEvent(String content) {
        super(new Object());
        this.content = "汪汪汪:" + content;
    }

    public String getContent() {
        return content;
    }
}
