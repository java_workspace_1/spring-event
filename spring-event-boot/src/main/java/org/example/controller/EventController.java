package org.example.controller;

import lombok.extern.slf4j.Slf4j;
import org.example.event.CatEvent;
import org.example.event.DogEvent;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationEvent;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @Description event
 * @Author zhouhouliang
 * @Date 2024/2/19 17:09
 */

@Slf4j
@RequestMapping("/event")
@RestController
public class EventController {

    @Resource
    private ApplicationContext ioc;

    @GetMapping("/trigger/{type}")
    public String trigger(@PathVariable("type") String type, @RequestParam String word) {
        ApplicationEvent event = null;
        if (type.equals("cat")) {
            event = new CatEvent(word);
        } else if (type.equals("dog")) {
            event = new DogEvent(word);
        }
        ioc.publishEvent(event);
        return "ok";
    }
}
